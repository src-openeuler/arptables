Name:             arptables
Version:          0.0.5
Release:	  6
Summary:          Administration tool for arp packet filtering

License:          GPLv2+
URL:              https://git.netfilter.org/arptables/
Source0:          http://ftp.netfilter.org/pub/arptables/%{name}-%{version}.tar.gz
Source1:          arptables-helper
Source2:          arptables.service
# GNU GENERAL PUBLIC LICENSE is from :
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
Source3:          COPYING

BuildRequires:    gcc perl-generators systemd make
%systemd_requires

Obsoletes:        arptables_jf < 0.0.8-37
Provides:         arptables_jf = 0.0.8-37

%description
Arptables is used to set up, maintain, and inspect the tables of
ARP packet filter rules in the Linux kernel. Several different
tables may be defined. Each table contains a number of built-in
chains and may also contain user-defined chains.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1
cp %{SOURCE3} COPYING

%build
make all 'COPT_FLAGS=%{optflags}' 'LDFLAGS=%{build_ldflags}' %{_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT BINDIR=%{_sbindir} MANDIR=%{_mandir}
mkdir $RPM_BUILD_ROOT%{_libexecdir}/
install -p -D -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_libexecdir}/
install -p -D -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}/arptables.service
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
echo '# Configure prior to use' > $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}
ln -s arptables-legacy $RPM_BUILD_ROOT/%{_sbindir}/arptables

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
if [ $1 -eq 1 ] && [ -x /usr/sbin/arptables-legacy ] && [ ! -h /usr/sbin/arptables ];then
ln -s /usr/sbin/arptables-legacy /usr/sbin/arptables
fi
%systemd_postun_with_restart %{name}.service

%posttrans
if [ -x /usr/sbin/arptables-legacy ] && [ ! -h /usr/sbin/arptables ];then
ln -s /usr/sbin/arptables-legacy /usr/sbin/arptables
fi

%files
%license COPYING
%{_libexecdir}/%{name}-helper
%{_unitdir}/%{name}.service
%{_sbindir}/%{name}*
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%exclude %{_initrddir}/%{name}

%files help
%{_mandir}/man8/*.gz

%changelog
* Mon Nov 28 2022 yanglu <yanglu72@h-partners.com> - 0.0.5-6
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the compilation dependency of make

* Sat Jun 25 2022 gaihuiying <eaglegai@163.com> - 0.0.5-5
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix post error when update versions

* Sat Aug 28 2021 gaihuiying <gaihuiying1@huawei.com> - 0.0.5-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix arptables service failed

* Thu Mar 25 2021 liulong <liulong20@huawei.com> - 0.0.5-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:modify the changelog description.

* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 0.0.5-2
- Type:requirements
- Id:NA
- SUG:NA
- DESC:remove sensitive keywords

* Thu Sep 10 2020 hanzhijun <hanzhijun1@huawei.com> - 0.0.5-1
- fix source url problem

* Tue Jun 02 2020 SimpleUpdate Robot <tc@openeuler.org>
- Update to version 0.0.5

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.0.4-16
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license files

* Thu Sep 5 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.0.4-15
- Package Init

